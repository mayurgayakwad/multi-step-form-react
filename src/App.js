import "./App.css";
import React, { useState } from "react";
import { Pg1 } from "./form/Pg1.form";
import { Pg2 } from "./form/Pg2.form";
import { Pg3 } from "./form/Pg3.form";
function App() {
  const [page, setPage] = useState(1);
  const Next = () => {
    setPage(page + 1);
  };
  const Back = () => {
    setPage(page - 1);
  };
  return (
    <>
      <div className="firstPageContainer ">
        {page == 1 ? (
          <Pg1 Next={Next} />
        ) : page == 2 ? (
          <Pg2 Next={Next} Back={Back} />
        ) : (
          <Pg3 Back={Back} />
        )}
      </div>
    </>
  );
}

export default App;
