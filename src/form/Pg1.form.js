import React, { useContext, useState } from "react";
import FormContext from "../formContext/formcontext";
import "../form/page1.css";

export const Pg1 = (props) => {
  const { firstName, setFirstName } = useContext(FormContext);
  const { lastName, setLirstName } = useContext(FormContext);
  const { dob, setDob } = useContext(FormContext);
  const { emailAdd, setEmailAdd } = useContext(FormContext);
  const { address, setAddress } = useContext(FormContext);
  const { FirstNameerror, setFirstNameError } = useContext(FormContext);
  const { LastNameerror, setLastNameError } = useContext(FormContext);
  const { dobError, setDobError } = useContext(FormContext);
  const { emailAddError, setEmailAddError } = useContext(FormContext);
  const { addressError, setAddressError } = useContext(FormContext);

  const FirstNameEvent = (event) => {
    setFirstName(event.target.value);
  };
  const LastNameEvent = (event) => {
    setLirstName(event.target.value);
  };
  const DobEvent = (event) => {
    setDob(event.target.value);
  };
  const EmailEvent = (event) => {
    setEmailAdd(event.target.value);
  };
  const AddressEvent = (event) => {
    setAddress(event.target.value);
  };

  const validate = (event) => {
    let flag = true;
    if (firstName.length == 0) {
      setFirstNameError(true);
      flag = false;
    } else {
      setFirstNameError(false);
    }
    if (lastName.length == 0) {
      setLastNameError(true);
      flag = false;
    } else {
      setLastNameError(false);
    }
    if (dob.length == 0) {
      setDobError(true);
      flag = false;
    } else {
      setDobError(false);
    }

    if (emailAdd.length == 0 || !emailAdd.includes("@")) {
      setEmailAddError(true);
      flag = false;
    } else {
      setEmailAddError(false);
    }

    if (address.length == 0) {
      setAddressError(true);
      flag = false;
    } else {
      setAddressError(false);
    }
    return flag;
  };

  return (
    <>
      <img
        className="image"
        src="https://img.freepik.com/free-vector/cyber-data-security-online-concept-illustration-internet-security-information-privacy-protection_1150-37330.jpg?w=996&t=st=1672204503~exp=1672205103~hmac=c60a3354d457427b5394d4d51f1be44a50accb3cc76546a33cf38910d35726b7"
      />
      <div className="rightDiv">
        <div className="RightTopDiv">
          <div className="box-page1 box1-page1">1</div>Sign Up
          <div className="box-page1 box2-page1">2</div>Massage
          <div className="box-page1 box3-page1">3</div>Checkbox
        </div>
        <br />
        <hr />
        <br />
        <h5>
          Step<span> 1/3</span>
        </h5>
        <h2>Sign UP</h2>
        <div className="container-page1">
          <label htmlFor="name">First Name</label>
          <label htmlFor="name">&nbsp;&nbsp;&nbsp;Last Name</label>
        </div>
        <div className="inputs">
          <input
            className="input"
            name="fName"
            type="text"
            value={firstName}
            onChange={FirstNameEvent}
          />

          <input
            className="input"
            name="lName"
            type="text"
            value={lastName}
            onChange={LastNameEvent}
          />
        </div>
        {FirstNameerror ? (
          <label className="FirstNameError" htmlFor="name">
            *First Name Require
          </label>
        ) : (
          ""
        )}
        <br />
        {LastNameerror ? (
          <label className="laNameError">*Last Name Require </label>
        ) : (
          ""
        )}
        <br />
        <div className="container-page1">
          <label htmlFor="name">Date of Birth </label>
          <label htmlFor="name">Email Address</label>
        </div>
        <div className="inputs">
          <input
            className="input"
            name="Dob"
            type="date"
            value={dob}
            onChange={DobEvent}
          />

          <input
            className="input"
            name="Email"
            type="email"
            value={emailAdd}
            onChange={EmailEvent}
          />
        </div>
        {dobError ? (
          <label className="FirstNameError" htmlFor="name">
            *DOB Require
          </label>
        ) : (
          ""
        )}
        <br />
        {emailAddError ? (
          <label className="EmailError" htmlFor="name">
            *Valid Email Require
          </label>
        ) : (
          ""
        )}
        <label className="addresslabel">Address</label>
        <input
          type="text"
          name="Address"
          className="addressinput"
          value={address}
          onChange={AddressEvent}
        />
        <br />
        {addressError ? (
          <p style={{ color: "red", margin: "0px" }}>*Address Require</p>
        ) : (
          ""
        )}
        <br />
        <br /> <hr />
        <button
          onClick={() => {
            if (validate()) {
              props.Next();
            }
          }}
        >
          Next Step
        </button>
      </div>
    </>
  );
};
