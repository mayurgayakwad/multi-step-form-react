import React, { useContext } from "react";
import FormContext from "../formContext/formcontext";
import "../form/page2.css";

export const Pg2 = (props) => {
  const { message, setMessage } = useContext(FormContext);
  const { messageError, setMessageError } = useContext(FormContext);
  const MassageEvent = (event) => {
    setMessage(event.target.value);
  };

  const validate = () => {
    let flag = true;
    if (message.length < 20) {
      setMessageError(true);
      flag = false;
    } else {
      setMessageError(false);
    }
    return flag;
  };
  return (
    <>
      <img
        className="image"
        src="https://img.freepik.com/free-vector/cyber-data-security-online-concept-illustration-internet-security-information-privacy-protection_1150-37328.jpg?w=826&t=st=1672143598~exp=1672144198~hmac=6a72a8c7eeb80de2ac7e6691d5dc2d10fe9c281bdba5908abbb7a239068b801b"
      />
      <div className="rightDiv">
        <div className="RightTopDiv">
          <div class="box-page2 box1-page2"></div>Sign Up
          <div class="box-page2 box2-page2">2</div>Massage
          <div class="box-page2 box3-page2">3</div>Checkbox
        </div>
        <br />
        <hr />
        <br />
        <h5>
          Step<span> 2/3</span>
        </h5>
        <h2>Message</h2>
        <label for="name" className="message">
          Message
        </label>
        <textarea
          className="text-area"
          value={message}
          onChange={MassageEvent}
        ></textarea>
        {messageError ? (
          <p style={{ color: "red" }}>Massage Field Require Min 20 char.</p>
        ) : (
          ""
        )}
        <div className="choices-page2">
          <input
            type="radio"
            checked="checked"
            id="one-page2"
            name="one"
            value={"The number one choices"}
          />
          <label for="one-page2" style={{ cursor: "pointer" }}>
            The number one choices
          </label>
          <input
            type="radio"
            id="two-page2"
            name="one"
            value={"The number two choices"}
          />
          <label for="two-page2" style={{ cursor: "pointer" }}>
            The number two choices
          </label>
        </div>
        <hr />
        <div className="buttonDiv">
          <button
            type="click"
            className="back-btn"
            onClick={() => {
              props.Back();
            }}
          >
            Back
          </button>
          <button
            onClick={() => {
              if (validate()) {
                props.Next();
              }
            }}
            className="next-btn"
          >
            Next Step
          </button>
        </div>
      </div>
    </>
  );
};
