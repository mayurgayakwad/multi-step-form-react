import React from "react";
import "../form/page3.css";
export const Pg3 = (props) => {
  const onSubmits = (event) => {
    event.preventDefault();
    alert("form submitted");
    window.location.reload(false);
  };
  return (
    <>
      <img
        className="image"
        src="https://img.freepik.com/free-vector/cyber-data-security-online-concept-illustration-internet-security-information-privacy-protection_1150-37330.jpg?w=996&t=st=1672204503~exp=1672205103~hmac=c60a3354d457427b5394d4d51f1be44a50accb3cc76546a33cf38910d35726b7"
        alt="image"
      />
      <div className="rightDiv">
        <div className="RightTopDiv">
          <div class="box-page3 box1-page3">{}</div>Sign Up
          <div class="box-page3 box2-page3"></div>Massage
          <div class="box-page3 box3-page3">3</div>Checkbox
        </div>
        <br />
        <hr />
        <br />
        <h5>
          Step<span> 3/3</span>
        </h5>
        <h2>Checkbox</h2>
        <form onSubmit={onSubmits}>
          <div className="container">
            <label for="male" name="gender">
              <svg
                className="maleSvg"
                xmlns="http://www.w3.org/2000/svg"
                width="50"
                height="50"
                viewBox="0 0 24 26"
              >
                <path d="M16.5 14.5c0 .828-.56 1.5-1.25 1.5s-1.25-.672-1.25-1.5.56-1.5 1.25-1.5 1.25.672 1.25 1.5zm-7.75-1.5c-.69 0-1.25.672-1.25 1.5s.56 1.5 1.25 1.5 1.25-.672 1.25-1.5-.56-1.5-1.25-1.5zm3.25 8.354c2.235 0 3-2.354 3-2.354h-6s.847 2.354 3 2.354zm12-6.041c0 1.765-.985 3.991-3.138 4.906-2.025 3.233-4.824 5.781-8.862 5.781-3.826 0-6.837-2.548-8.862-5.781-2.153-.916-3.138-3.142-3.138-4.906 0-2.053.862-3.8 2.71-3.964.852-9.099 8.57-8.408 9.837-10.849.323.559.477 1.571-.02 2.286.873-.045 2.344-1.304 2.755-2.552.754.366 1.033 1.577.656 2.354.542-.103 2.187-1.15 3.062-2.588.688 1.563.026 3.563-.708 4.771l-.012.001c1.796 1.707 2.781 4.129 3.01 6.576 1.859.165 2.71 1.917 2.71 3.965zm-2.58-1.866c-.235-.152-.531-.115-.672-.053-.56.25-1.214-.062-1.372-.66l-.001.016c-.333-2.604-1.125-4.854-2.611-5.565-6.427 7.009-10.82-.914-11.94 3.529-.101.582-.166 1.172-.166 1.766 0 .719-.743 1.209-1.406.914-.14-.062-.437-.1-.672.053-1 .651-.894 4.184 1.554 5.012.224.076.413.228.535.43 2.447 4.053 5.225 5.111 7.331 5.111 3.288 0 5.615-2.269 7.332-5.111.122-.202.312-.354.535-.43 2.447-.828 2.553-4.361 1.553-5.012z" />
              </svg>
            </label>
            <input
              name="gender"
              type="radio"
              className="checkForMale"
              id="male"
              checked="checked"
            />
            <label for="female" name="gender">
              <svg
                className="femaleSvg"
                xmlns="http://www.w3.org/2000/svg"
                width="50"
                height="50"
                viewBox="0 0 26 24"
              >
                <path d="M17.5 12.5c0 .828-.56 1.5-1.25 1.5s-1.25-.672-1.25-1.5.56-1.5 1.25-1.5 1.25.672 1.25 1.5zm-7.75-1.5c-.69 0-1.25.672-1.25 1.5s.56 1.5 1.25 1.5 1.25-.672 1.25-1.5-.56-1.5-1.25-1.5zm3.25 8.354c2.235 0 3-2.354 3-2.354h-6s.847 2.354 3 2.354zm13 3.639c-2.653 1.714-5.418 1.254-6.842-1.488-1.672 1.505-3.706 2.487-6.158 2.487-2.53 0-4.517-.91-6.184-2.445-1.431 2.702-4.178 3.15-6.816 1.446 4.375-1.75-2.729-11.813 4.104-19.375 2.282-2.525 5.472-3.618 8.896-3.618s6.614 1.093 8.896 3.618c6.833 7.562-.271 17.625 4.104 19.375zm-5.668-6.111c.122-.202.312-.354.535-.43 2.447-.828 2.554-4.361 1.554-5.012-.235-.152-.531-.115-.672-.053-.664.295-1.406-.194-1.406-.914 0-.471-.034-1.001-.096-1.473h-10.101c-.813-1.021-.771-2.945-.396-4.57-.903.982-1.693 3.249-1.875 4.57h-2.121c-.062.472-.096 1.002-.096 1.473 0 .72-.742 1.209-1.406.914-.141-.062-.436-.1-.672.053-1 .651-.893 4.184 1.554 5.012.224.076.413.228.535.43 1.709 2.829 4.015 5.111 7.332 5.111 3.316 0 5.623-2.283 7.331-5.111z" />
              </svg>
            </label>
            <input
              name="gender"
              type="radio"
              className="checkForFemale"
              id="female"
            />
          </div>
          <div className="radio-btn">
            <input type="radio" id="one-page3" name="one" />
            <label for="one-page3">I want to add this option.</label>
            <br />
            <input type="radio" id="two-page3" name="one" checked="checked" />
            <label for="two-page3">
              Let me click on this checkbox and choose some cool stuff.
            </label>
          </div>
          <hr />
          <div className="buttonDiv">
            <button
              type="click"
              className="back-btn"
              onClick={() => {
                props.Back();
              }}
            >
              Back
            </button>
            <button type="submit" className="next-btn">
              Submit
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
